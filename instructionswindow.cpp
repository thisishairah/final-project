#include "instructionswindow.h"
#include "ui_instructionswindow.h"

InstructionsWindow::InstructionsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::InstructionsWindow)
{
    ui->setupUi(this);
}

InstructionsWindow::~InstructionsWindow()
{
    delete ui;
}
