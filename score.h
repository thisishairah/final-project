#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>

class score: public QGraphicsTextItem{
public:
    score(QGraphicsItem * parent = 0);      //creates score
    int getScore();     //returns score
    void increase();    //increases score
private:
    int currentScore;
};

#endif // SCORE_H
