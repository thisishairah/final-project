#ifndef INSTRUCTIONSWINDOW_H
#define INSTRUCTIONSWINDOW_H

#include <QMainWindow>

namespace Ui {
class InstructionsWindow;
}

class InstructionsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit InstructionsWindow(QWidget *parent = 0);
    ~InstructionsWindow();

private:
    Ui::InstructionsWindow *ui;
};

#endif // INSTRUCTIONSWINDOW_H
