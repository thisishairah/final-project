#include "score.h"
#include <QFont>
#include "lives.h"

//this creates the score and shows it
score::score(QGraphicsItem* parent): QGraphicsTextItem(parent) {
    //set initial score equal to 0
    currentScore = 0;

    //print out the score
    setPlainText("Score: " + QString::number(currentScore));
    setFont(QFont("times", 11));
}


//this returns the current score
int score::getScore(){
    return currentScore;
}


//this increases the score and reveals the new score
void score::increase() {
    currentScore++;
    setPlainText("Score: " + QString::number(currentScore));
}
