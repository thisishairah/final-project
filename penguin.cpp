#include "penguin.h"
#include "items.h"
#include "game.h"
#include <QKeyEvent>

extern Game * currentGame;

//creates the cute little penguin
Penguin::Penguin(QGraphicsItem * parent): QObject(),QGraphicsPixmapItem(parent) {
    setPixmap(QPixmap(":/image/Penguin.png"));
}

//adds the items to the scene
void Penguin::itemsAppear(){
    Items * item = new Items();
    scene()->addItem(item);
}
