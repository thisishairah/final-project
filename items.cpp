#include "items.h"
#include <QGraphicsScene>
#include <QList>
#include "game.h"
#include <ctime>
#include <random>
#include <chrono>
#include <QTimer>
#include "penguin.h"

#define SEED (unsigned int)std::chrono::system_clock::now().time_since_epoch().count()
#define DEFAULT std::default_random_engine
#define DUNIF(T) std::uniform_int_distribution<T>

extern Game * currentGame;

Items::Items(QGraphicsItem * parent): QObject(), QGraphicsPixmapItem(parent){
    DEFAULT randomNumber(SEED);
    DUNIF(int) unif(100,650);

    setPos(800,unif(randomNumber));

    DUNIF(int) randomItem(0,5);
    incomingItem = randomItem(randomNumber);

    if (incomingItem == 0) {
        setPixmap(QPixmap(":/image/fish1.png"));
    } if (incomingItem == 1) {
        setPixmap(QPixmap(":/image/fish2.png"));
    } if (incomingItem == 2) {
        setPixmap(QPixmap(":/image/shrimp.png"));
    } if (incomingItem == 3) {
        setPixmap(QPixmap(":/image/jellyfish.png"));
    } if (incomingItem == 4) {
        setPixmap(QPixmap(":/image/shark.png"));
    } else if (incomingItem == 5) {
        setPixmap(QPixmap(":/image/whale.png"));
    }

    QTimer * speed = new QTimer(this);
    connect (speed, SIGNAL(timeout()),this,SLOT(itemEnters()));

    speed->start(30);

}

//if the penguin touches the item, it results in either decrease of lives or increase of score or penguin is unable to move
void Items::itemEnters(){
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for(int i = 0, n = colliding_items.size(); i < n; ++i) {
        if(typeid(*(colliding_items[i])) == typeid(Penguin)){
            //decrease lives if penguin encounters predator!
            if (incomingItem == 4 || incomingItem == 5) {
                currentGame->currentLives->decrease();
                scene()->removeItem(this);
                delete this;
                return;
            //increase score if penguin encounters food
            } else {
                currentGame->currentScore->increase();
                scene()->removeItem(this);
                delete this;
                return;
            }
        }
    }

    if (currentGame->level == 2) {
        setPos(x()-10,y());
    } if (currentGame->level == 3) {
        setPos(x()-15, y());
    } else {
        setPos(x()-5,y());
    }
}
