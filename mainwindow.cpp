#include "mainwindow.h"
#include "ui_mainwindow.h"

/**
 * @brief MainWindow::MainWindow
 * @param parent
 * This shows the main window for the game
**/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
}


/**
 * @brief MainWindow::~MainWindow
 * This deletes the gui
**/
MainWindow::~MainWindow() {
    delete ui;
}


//This sets up the instructions as i, which reveals the instructions window
void MainWindow::showInstructions(InstructionsWindow* i) {
    instructions = i;
}


//This sets the game
void MainWindow::beginGame(Game* g) {
    currentGame = g;
}


//This starts the game when clicked and hides the title screen
void MainWindow::on_pushButton_clicked(){
    currentGame->start();
    hide();
}


//This shows the new window with the instructions
void MainWindow::on_pushButton_2_clicked(){
    instructions->show();
}


//when the radio button is clicked, this sets the game as easy
void MainWindow::on_radioButton_clicked(bool checked) {
    currentGame->level = 1;
}


//when the radio button is clicked, this sets the game as medium
void MainWindow::on_radioButton_2_clicked(bool checked) {
    currentGame->level = 2;
}


//when the radio button is clicked, this sets the game as hard
void MainWindow::on_radioButton_3_clicked(bool checked) {
    currentGame->level = 3;
}

