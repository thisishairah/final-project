/**
    * Hello! This is a game called Penguin Eats.
    * The goal of the game is to help the penguin eat their food (fish or shrimp)
    * However, be careful of the jellyfish because they can prevent the penguin from moving temporarily!
    * And prevent the penguin from being eaten by a shark or a whale
    * Penguin appreciates your help!
    *
    *
    * *I accidentally encountered a bug in this program. It was working fine before, but I'm not sure what I did.
    * *So, currently it does not work.
    * *However, I hope that you are able to see my thinking process throughout the game.
    *
    *
    * Shairah Carpio        12/16/2016
    * PIC10C Final Project
    * Professor Salazar
    *
**/

#include "mainwindow.h"
#include <QApplication>
#include "game.h"
#include "gameoverwindow.h"
#include "instructionswindow.h"

int main(int argc, char *argv[])
{
    QApplication program(argc, argv);

    //creates the main window
    MainWindow w;

    //pops up the instructions window
    InstructionsWindow i;

    //shows the game over window
    GameOverWindow o;

    //creates a new game. This is supposed to create the game, but I have no idea why it won't work?
    currentGame = new Game();
    w.beginGame(currentGame);
    w.showInstructions(&i);
    currentGame->showGameOver(&o);

    return program.exec();
}
