#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "penguin.h"
#include "score.h"
#include "lives.h"
#include "gameoverwindow.h"

class Game: public QGraphicsView {
public:
    Game();

    void setting();

    //starts and ends the game
    void start();
    void end();

    //show the gameover screen
    void showGameOver(GameOverWindow* o);

    //moves the penguin in the game
    void keyPressEvent(QKeyEvent * move);

    //variables
    QGraphicsScene * scene;
    Penguin * penguin;
    score * currentScore;
    Lives * currentLives;
    GameOverWindow* endGame;
    int level;
};

#endif // GAME_H
