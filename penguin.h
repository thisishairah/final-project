#ifndef PENGUIN_H
#define PENGUIN_H

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QObject>

class Penguin: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Penguin(QGraphicsItem * parent = 0);
public slots:
    void itemsAppear();
};

#endif // PENGUIN_H
