#include "game.h"
#include <QGraphicsRectItem>
#include <QTimer>
#include <QGraphicsTextItem>
#include <QKeyEvent>
#include "penguin.h"
#include "items.h"
#include <QGraphicsView>
#include <QImage>
#include "lives.h"
#include "score.h"
#include <QWidget>

/**
 * @brief Game::Game
 * @param parent
 * this game class sets up the scene
**/
Game::Game(){
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1000,750);

    //background image
    setBackgroundBrush(QImage(":/image/background.png"));
    setting();
}

//sets scene such that it has a fixed size and no scrollbars
void Game::setting(){
    setScene(scene);

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setFixedSize(1000,750);
}

//starts the game
void Game::start() {

    //creates a penguin
    penguin = new Penguin();

    //sets the penguin's initial position
    penguin->setPos(250,250);

    //makes the penguin focusable
    penguin->setFlag(QGraphicsItem::ItemIsFocusable);
    penguin->setFocus();

    //adds the penguin to the scene
    scene->addItem(penguin);

    //creates the score (initially 0)
    currentScore = new score();

    //adds the score to the scene
    scene->addItem(currentScore);

    //creates the lives (initially 3)
    currentLives = new Lives();
    currentLives->game(this);

    //shows the lives on screen
    currentLives->setPos(currentLives->x(), currentLives->y()+25);

    //adds the lives on the scene
    scene->addItem(currentLives);

    //creates a QTimer such that the speed of the items vary based on the difficulty level
    QTimer * speed = new QTimer();
    QObject::connect(speed, SIGNAL(timeout()), penguin, SLOT(itemsAppear()));

    //if player selected medium level
    if (level == 2) {
        speed->start(1200);
    //if player selected hard level
    } else if (level == 3) {
        speed->start(750);
    //otherwise, the default is easy
    } else {
        speed->start(2000);
    }

    //show everything on the scene
    show();
}


//This ends the game, revealing the GameOverWindow and hiding the game window
void Game::end() {
    endGame->show();
    hide();
}

//This sets the GameOverWindow
void Game::showGameOver(GameOverWindow* o) {
    endGame = o;
}

//this moves the penguin
void Game::keyPressEvent(QKeyEvent *move) {
    if (move->key() == Qt::Key_Down) {
        penguin->moveBy(0,10);
    } if (move->key() == Qt::Key_Up) {
        penguin->moveBy(0,-10);
    } if (move->key() == Qt::Key_Right) {
        penguin->moveBy(10,0);
    } else if (move->key() == Qt::Key_Left) {
        penguin->moveBy(-10,0);
    }
}
