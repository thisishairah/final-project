#ifndef ITEMS_H
#define ITEMS_H

#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>

class Items: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Items(QGraphicsItem * parent = 0);

    int incomingItem;

public slots:
    void itemEnters();

};

#endif // ITEMS_H
