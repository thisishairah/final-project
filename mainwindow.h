#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "instructionswindow.h"
#include "game.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void beginGame (Game* g);
    void showInstructions(InstructionsWindow* i);

private slots:
    void on_pushButton_clicked();   //start button

    void on_pushButton_2_clicked();     //instructions button

    void on_radioButton_clicked(bool checked);     //easy

    void on_radioButton_2_clicked(bool checked);    //medium

    void on_radioButton_3_clicked(bool checked);    //hard

private:
    Ui::MainWindow *ui;     //shows the window
    Game* currentGame;             //creates a game
    InstructionsWindow* instructions;       //instructions page
};

#endif // MAINWINDOW_H
