#include "lives.h"
#include "game.h"
#include "score.h"
#include <QFont>

Lives::Lives(QGraphicsItem * parent): QGraphicsTextItem(parent){
    //sets initial lives to 3
    currentLives=3;

    //prints out the number of lives penguin has
    setPlainText("Lives: " + QString::number(currentLives));
    setFont(QFont("times", 11));
}

//returns the number of lives
int Lives::getLives(){
    return currentLives;
}

//decreases penguin's lives and prints out new lives count
void Lives::decrease(){
    currentLives--;

    if (currentLives < 1) {
        currentGame->end();
    }
    setPlainText("Lives: " + QString::number(currentLives));
}

//sets the current game
void Lives::game(Game *g){
    currentGame = g;
}

