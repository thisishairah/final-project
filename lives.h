#ifndef LIVES_H
#define LIVES_H

#include <QGraphicsTextItem>

class Game;

class Lives: public QGraphicsTextItem{
public:
    Lives(QGraphicsItem * parent = 0);
    int getLives();
    void decrease();
    void game (Game* g);
private:
    int currentLives;
    Game* currentGame;
};

#endif // LIVES_H
