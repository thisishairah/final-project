#-------------------------------------------------
#
# Project created by QtCreator 2016-12-15T10:17:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PenguinEats
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    instructionswindow.cpp \
    game.cpp \
    penguin.cpp \
    score.cpp \
    lives.cpp \
    gameoverwindow.cpp \
    items.cpp

HEADERS  += mainwindow.h \
    instructionswindow.h \
    game.h \
    penguin.h \
    score.h \
    lives.h \
    gameoverwindow.h \
    items.h

FORMS    += mainwindow.ui \
    instructionswindow.ui \
    gameoverwindow.ui

RESOURCES += \
    image.qrc
